package lesson6;

import java.util.HashMap;
import java.util.Map;

public class MapSolution {

    public static void main(String[] args) {
        // Map -> interface, SortedMap (I), NavigableMap (I)
        // HashMap, LinkedHashMap, TreeMap, ConcurrentHashMap
        // Map KEY (unique) : VALUE (non-unique)
        // Set KEY
        HashMap<String, Integer> map = new HashMap<>();
        map.put("Sergey", 19); // .put позволяет добавить в map
        System.out.println(map.get("Sergey")); // .get по ключу сразу вернет значение
        // map.clear(); // очистить map
        System.out.println(map.containsKey("Sergey"));
        map.replace("Sergey", 21); // replace меняет значение
        System.out.println(map);
        map.put("Sergey", map.get("Sergey") + 123);
        // map.merge("Sergey", 123, (a, b) -> a + b); // Каждый раз создаем с нуля
        // map.merge("Sergey", 123, Integer::sum); // Method Reference | Java не тратит время на создание лямбда-выражения, а просто использует уже реализацию
        System.out.println(map);
        map.remove("Sergey"); // remove по ключу удаляет из map

        map.put("q", 2);
        map.put("s", 3);
        map.put("q", 5);
        map.put("xx", 5);
        // Значение перетираются
        System.out.println(map);
        System.out.println(map.keySet()); // Set из ключей
        System.out.println(map.values());// Collection из значений
        HashMap<String, Integer> newMap = new HashMap<>();
        newMap.putAll(map);
        System.out.println(newMap);

        // Task: вывести ключи и значения map
        // 1
        for (int i = 0; i < map.keySet().toArray().length; i++) {
            System.out.println("Ключ " + map.keySet().toArray()[i]);
            System.out.println("Значение " + map.get(map.keySet().toArray()[i]));
        }
        System.out.println();
        // 2
        for (String s : map.keySet()) { // forEach (позволяет бежать не по индексу, а сразу по значениям
            System.out.println("Ключ " + s);
            System.out.println("Значение " + map.get(s));
        }
        // 3
        // map.entrySet() это и ключи, и значения
        // Map.Entry -> интерфейс, без которого НЕ пробежаться по entrySet
        System.out.println();
        for (Map.Entry<String, Integer> m : map.entrySet()) {
            System.out.println("Ключ " + m.getKey());
            System.out.println("Значение " + m.getValue());
        }
    }
}
