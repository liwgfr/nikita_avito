package lesson6;

import java.util.Scanner;

public class Main {
    // ENUM -> специальный тип класса, призванный для хранения каких-либо констант

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println(Months.SEPTEMBER);
        System.out.println(Months.valueOf("MAY")); // "May" -> Months.May
        System.out.println(Months.MARCH.name()); // аналог toString
        System.out.println(Months.MARCH.ordinal()); // порядок
        System.out.println(Months.values()[0]);
        // Вводится номер месяца -> вывести его текстом и наоборот
        System.out.println("Введите номер месяца:");
        int n = in.nextInt();
        for (int i = 0; i < Months.values().length; i++) {
            if (Months.values()[i].getNumberOfMonth() == n) {
                System.out.println(Months.values()[i].name());
            }
        }
        System.out.println("Введите название месяца: ");
        String s = in.next().toUpperCase();
        System.out.println(Months.valueOf(s).getNumberOfMonth());
        for (int i = 0; i < Months.values().length; i++) {
            if (Months.values()[i].name().equals(s)) {
                System.out.println(Months.values()[i].getNumberOfMonth());
            }
        }
    }
}

enum Months {
    // Структура:
    // Сначала строго (!) идут Enum (перечисления).
    // Записываются они в виде текста (кроме бан-символов: @ - / \ ``~...) (_ разрешен)
    // Перечисляются через ,
    // По умолчанию пустого конструктора нет
    // Суть конструктора в enum заключается в заранее подготовленной информации
    JANUARY(1),
    FEBRUARY(2),
    MARCH(3),
    APRIL(4),
    MAY(5),
    JUNE(6),
    JULY(7),
    AUGUST(8),
    SEPTEMBER(9),
    OCTOBER(10),
    NOVEMBER(11),
    DECEMBER(12);

    private int numberOfMonth;

    Months(int numberOfMonth) {
        this.numberOfMonth = numberOfMonth;
    }

    public int getNumberOfMonth() {
        return numberOfMonth;
    }
    //    @Override
//    public String toString() { // It's not recommended to override toString
//        return "qwe";
//    }

}





