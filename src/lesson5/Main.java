package lesson5;

import java.util.Arrays;
import java.util.Comparator;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {
        // HashSet
        // "qwe"
        // "qwe"
        // "asd"
        // "ewq"
        // "qwe"
        // {asd, ewq, qwe}

        // LinkedHashSet
        // "qwe"
        // "qwe"
        // "asd"
        // "ewq"
        // "qwe"
        // {qwe, asd, ewq}

        TreeSet<Integer> set = new TreeSet<>(Comparator.reverseOrder());
        // Сортировка по умолчанию - возрастающая
        // Однако, благодаря SortedSet мы можем самостоятельно изменить дефолтную сортировку на любую другую
        // Comparator.reverseOrder() -> показывает, что сортировка по убыванию
        // Comparator.naturalOrder() -> показывает, что сортировка по возрастанию
        // В реализации TreeSet (TreeMap) -> находится структура данных Красно-черное дерево (Красно-черное АВЛ дерево)
        set.add(2);
        set.add(1);
        set.add(-1);
        set.add(-100);
        set.add(5);
        set.add(10);
        set.add(23);
        System.out.println(set);
        System.out.println(set.floor(-2)); // Возвращает равное или меньшее значение, чем то, которое мы передали
        System.out.println(set.ceiling(6)); // Возвращает равное или большее значение, чем то, которое мы передали
        System.out.println(set.tailSet(3));
        System.out.println(set);

        // Exceptions
        // Проверяемые и непроверяемые исключения
//         System.out.println("hello" * 5); // Пример проверяемого исключения
        // К непроверяемым относятся: Error (fatal), RuntimeException, OutOfMemoryExceptions, StackOverFlowException -> переполнение стэка (например, во время рекурсии), LinkageError, IndexOutOfBounds...
        // К проверяемым: IOException (FileNotFoundException), SocketException, IncompatibleTypesExceptions
        try { // аналог if else, где else выполняется только тогда, когда ловится определенное исключение
            System.out.println(1 / 0); // Пример непроверяемого исключения
            // code
        } catch (ArithmeticException qweqwe) {
            System.out.println("Произошло исключение " + Arrays.toString(qweqwe.getStackTrace()));
            System.out.println("Что-то пошло не так");
        }
    }
}
