package lesson4;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;

public class YoursHomework {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int degree = 10;
        int n;
        int x;
        int idx;
        String number = "";
        String idxSet;
        boolean exit = false;
        int countElementsArray = (int) (Math.random() * 10 + 1);
        ArrayList<HashSet<String>> list = new ArrayList<>();
        for (int i = 0; i < countElementsArray; i++) {
            HashSet<String> set = new HashSet<>();
            int countElementsHashSet = (int) (Math.random() * 10 + 1);
            for (int j = 0; j < countElementsHashSet; j++) {
                int minus = (int) (Math.random() * 2 + 1);
                if (minus == 1) {
                    number += "-";
                    // Integer -> ссылочный тип данных
                    // Integer x <=> int x
                }
                for (int q = 0; q < degree; q++) {
                    x = (int) (Math.random() * 10 + 1);
                    int y = x;
                    number += y; // number = number + y; -> строка + число -> строка + строка
                }
                set.add(number);
                number = "";
            }
            list.add(set);
        }
        while (!exit) {
            System.out.println("1. Добавить в определенный элемент ArrayList значение в HashSet");
            System.out.println("2. Удалить из определенного элемента ArrayList'a значение в HashSet");
            System.out.println("3. Удалить HashSet из определенного элемента ArrayList'a");
            System.out.println("4. Показать все HashSet'ы из ArrayList");
            System.out.println("5. Выйти из цикла");
            n = in.nextInt();
            switch (n) {
                case 1:
                    System.out.println("В какой элемент ArrayList в котором насчитывается " + list.size() + " HashSetов будем вносить изменения?");
                    idx = in.nextInt();
                    if (idx <= list.size() & idx > 0) {
                        idx--;
                        System.out.println("Какое значение добавить в HashSet?");
                        idxSet = in.next();
                        HashSet<String> setx = list.get(idx);
                        setx.add(idxSet);
                        list.set(idx, setx);
                        System.out.print("Обновленный HashSet: ");
                        System.out.print(setx.size());
                        System.out.println(setx);
                    } else {
                        System.out.println("Такого элемента ArrayList не существует.");
                    }
                    break;
                case 2:
                    System.out.println("Из какого элемента ArrayList в котором насчитывается " + list.size() + " HashSetов будем удалять значение?");
                    idx = in.nextInt();
                    if (idx <= list.size() & idx > 0) {
                        idx--;
                        System.out.println("Какое значение удалисть из HashSetа?");
                        idxSet = in.next();
                        HashSet<String> setx = list.get(idx);
                        if (setx.contains(idxSet)) {
                            setx.remove(idxSet);
                            list.set(idx, setx);
                        } else {
                            System.out.println("No element");
                        }
                    } else {
                        System.out.println("Такого элемента ArrayList не существует.");
                    }
                    break;
                case 3:
                    System.out.println("Какой элемент удалить из ArrayList в котором насчитывается " + list.size() + " HashSetов.");
                    idx = in.nextInt();
                    if (idx <= list.size() & idx > 0) {
                        list.remove(idx - 1);
                        System.out.println(idx + "-й HashSet удалили из ArrayList.");
                    } else {
                        System.out.println("Такого HashSetа нет в ArrayList.");
                    }
                    break;
                case 4:
                    System.out.println(list);
                    break;
                case 5:
                    exit = true;
                    System.out.println("Программа закончила свою работу.");
                    break;
                default:
                    System.out.println("Вы ввели неверный пункт меню. Выберите еще раз:");
            }
            ;
        }
        ;
        /* todo
            Есть ArrayList, где каждый элемент это HashSet из целых чисел (int) + extra task: числа (в любом представлении) значение числа от -2^100...2^100
            Необходимо реализовать следующее:
            Должно быть небольшое меню для пользователя:
                1. Добавить в определенный элемент ArrayList значение в HashSet (после этого вывести этот HashSet)
                2. Удалить из определенного элемента ArrayList'a значение в HashSet (если оно там есть, если нет - вывод "No element")
                3. Удалить HashSet из определенного элемента ArrayList'a
                4. Показать все HashSet'ы из ArrayList
            Input Example:
            1
            2 // {new HashSet(), new HashSet(), new HashSet({123})}
            123
            Output:
            {123}
         */
    }
}