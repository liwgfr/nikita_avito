package lesson4;

public class WorkWithFiles implements RealisationChooser {

    @Override
    public void name() {
        System.out.println("I work with File");
    }

    public void files() {
        System.out.println("files");
    }

    @Override
    public String toString() {
        return "WorkWithFiles{}";
    }
}
