package lesson4;

import java.util.*;

public class CollectionLesson {
    // Collection API -> были введены начиная с Java 5+
    // API -> Application Programming Interface
    // Набор каких-то методов, которые упрощают взаимодействие с библиотекой
    // Collection -> структуры данных (List, Queue, Stack, Map...)
    // List, Queue, Set... (кроме Map) -> интерфейсе
    // Во главе этих интерфейсов стоит Collection (I)
    // ArrayList (C) -> реализует интерфейс List. В контексте Java это динамический массив (массив без строгого размера -> увеличивается по мере заполнения)
    // У ArrayList есть различные полезные методы
    // add(...) -> добавляет в ArrayList значение
    // .get(int idx) -> возвращает значение по индексу
    // toString() -> ArrayList в строку
    // list.contains(5) -> возвращает true, если в ArrayList есть 5
    // list.remove(int idx) -> удаляет по индексу элемент
    // list.size() -> возвращает размер коллекции (аналог array.length)

    public static void main(String[] args) {
        // Для коллекций можно (и даже желательно)
        // указывать тип данных с которыми работаем <String> или <Integer> или <Double>
        // или <Character> (char)
        ArrayList<Integer> list = new ArrayList<>(); // по умолчанию Object не int, а Integer
        // int -> Integer
        // short -> Short
        // double -> Double
        // float -> Float
        // boolean -> Boolean
        // byte -> Byte
        // char -> Character
        // long -> Long
        // <>
        list.add(3);
        list.add(1);
        list.add(2);
        list.add(5);
        list.add(10);
        list.add(-2);
        list.add(3);
        // {3, 1, 2, 5, 10, -2, 3} -> Сортировка по возрастанию

        int[] a1 = {3, 1, 2, 5, 10, -2, 3};
        for (int i = 0; i < a1.length; i++) { // Bubble Sort
            for (int j = 0; j < a1.length; j++) {
                if (a1[i] < a1[j]) {
                    int temp = a1[i];
                    a1[i] = a1[j];
                    a1[j] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(a1));

        list.sort(Comparator.naturalOrder()); // Comparator
        System.out.println(list);

        int cnt = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) % 2 == 0) {
                cnt++;
            }
        }
        System.out.println(cnt);

        // LinkedList -> связный список
        LinkedList<Integer> list1 = new LinkedList<>();
        // ArrayList<Integer>
        // LinkedList<Integer>
        // {1, 2, 3} -> 1 -> 2 -> 3
        //.add(5)
        // {-200, 265456, 444, 5}
        // ArrayList эффективен когда в приоритете работа с индексом (get(i))
        // ArrayList.set(2, 10) -> for (...i)
        // remove
        // set
        // add(index, value)
        // LinkedList.set(2, 10) -> O(1)
        // LinkedList удобен и работает эффективен при вставке/удалении элементов. При этом get выполняется дольше

        // LinkedList эффективнее использовать когда часто вставляем/удаляем, во всех остальных случаях - ArrayList

        // CopyOnWriteArrayList -> эта штука для многопоточки

        // Set
        // Set - множество. То есть в Set не может быть повторяющихся объектов
        // "asb", "asb" -> {"asb"}
        // HashSet -> одна из реализаций интерфейса Set
        // Hash
        // add -> добавить в Set
        // remove -> удаляет объект
        // НЕТ get(index)!
        // в HashSet порядок ввода может отличаться от вывода
        HashSet<Integer> set = new HashSet<>();
        set.add(2);
        set.add(3);
        set.add(10);
        set.add(3);
        set.add(3);
        set.add(10);
        set.add(2);
        set.add(115);
        System.out.println(set);
        // set.size(); -> размер Set
        // HashSet.toArray() -> Object[]
        System.out.println(set.toArray()[2]);

        ArrayList<Integer> chetArray = new ArrayList<>();
        for (int i = 0; i < set.size(); i++) {
            int currValue = (int) set.toArray()[i];
            if (currValue % 2 == 0) {
                chetArray.add(currValue);
            }
        }
        chetArray.sort(Comparator.naturalOrder());
        System.out.println(chetArray);

        /* todo
            Есть ArrayList, где каждый элемент это HashSet из целых чисел (int) + extra task: числа (в любом представлении) значение числа от -2^100...2^100
            Необходимо реализовать следующее:
            Должно быть небольшое меню для пользователя:
                1. Добавить в определенный элемент ArrayList значение в HashSet (после этого вывести этот HashSet)
                2. Удалить из определенного элемента ArrayList'a значение в HashSet (если оно там есть, если нет - вывод "No element")
                3. Удалить HashSet из определенного элемента ArrayList'a
                4. Показать все HashSet'ы из ArrayList
            Input Example:
            1
            2 // {new HashSet(), new HashSet(), new HashSet({123})}
            123
            Output:
            {123}
         */
    }
}
