package lesson4;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // Интерфейс старше класса, который его реализует
        // Тип данных слева всегда важнее типа данных справа
        // Правый (специфичный, дочерний...) обобщился (то есть мы привели его к общему виду - то есть к родителю или интерфейсу)
        RealisationChooser work = new WorkWithFiles(); // Обобщили восходящее преобразование
        // Такой трюк работает со всеми классами, имеющими связь родитель-дочерний
        work.name();
        System.out.println(work); // Даже при обобщении, методы класса Object (toString, hashCode, equals...) будут браться из реализации дочернего (если они там имеются)
        ((WorkWithFiles) work).files(); // нисходящее преобразование (наоборот, от родительского до дочернего с помощью cast)
        Object o = new WorkWithFiles();
        System.out.println(o);

        // Дана строка "sdkfjn123213f k123sd231"
        // Необходимо вытащить число (то есть 123213123231 - не String, long/int)
        int result = new Integer(123); // классы оболочки автоматически конвертируются в примитивы и наоборот
        // Integer -> int unboxing
        // int -> Integer autoboxing
        String s = ((Integer) result).toString();
        System.out.println(result);
        System.out.println(s);

        String q = "sdkfjn123213f k123sd231";
        // 1)
        String res = "";
        for (int i = 0; i < q.length(); i++) {
            if (q.charAt(i) == '0' || q.charAt(i) == '1'
                    || q.charAt(i) == '2' || q.charAt(i) == '3'
                    || q.charAt(i) == '4' || q.charAt(i) == '5'
                    || q.charAt(i) == '6' || q.charAt(i) == '7'
                    || q.charAt(i) == '8' || q.charAt(i) == '9') {
                res += q.charAt(i);
            }
        }
        long answer = Long.parseLong(res);
        System.out.println("Long value is " + answer);

        // 2)
        res = "";
        q = q.replaceAll("[^0-9]", "");
        answer = Long.parseLong(q);
        System.out.println(answer);

        /* todo
            http://acm.sgu.ru/lang/problem.php?contest=2&problem=2038
         */
    }
}
