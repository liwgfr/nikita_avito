package lesson4;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;

/* todo
           Есть ArrayList, где каждый элемент это HashSet из целых чисел (int) + extra task: числа (в любом представлении) значение числа от -2^100...2^100
           Необходимо реализовать следующее:
           Должно быть небольшое меню для пользователя:
               1. Добавить в определенный элемент ArrayList значение в HashSet (после этого вывести этот HashSet)
               2. Удалить из определенного элемента ArrayList'a значение в HashSet (если оно там есть, если нет - вывод "No element")
               3. Удалить HashSet из определенного элемента ArrayList'a
               4. Показать все HashSet'ы из ArrayList
           Input Example:
           1
           2 // {new HashSet(), new HashSet(), new HashSet({123})}
           123
           Output:
           {123}
        */
public class Homework {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        ArrayList<HashSet<Integer>> ar = new ArrayList<>();
        while (true) {
            int n = in.nextInt();
            if (n == 1) {
                try {
                    int idx = in.nextInt();
                    ar.get(idx).add(in.nextInt());
                    System.out.println(ar.get(idx));
                } catch (IndexOutOfBoundsException e) {
                    HashSet<Integer> set = new HashSet<>();
                    set.add(in.nextInt());
                    ar.add(set);
                    System.out.println(ar.get(ar.size() - 1));
                }
            } else if (n == 2) {
                if (ar.size() == 0) {
                    System.out.println("No element");
                } else {
                    int idx = in.nextInt();
                    int value = in.nextInt();
                    if (ar.get(idx).contains(value)) {
                        ar.get(idx).remove(value);
                    } else {
                        System.out.println("No element");
                    }
                }
            } else if (n == 3) {
                if (ar.size() > 0) {
                    ar.remove(in.nextInt());
                }
            } else if (n == 4) {
                for (int i = 0; i < ar.size(); i++) {
                    System.out.println(ar.get(i));
                }
            } else {
                break;
            }
        }
    }
}
