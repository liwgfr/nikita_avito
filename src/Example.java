public class Example {
    public static void main(String[] args) {
        Example x = new Example();
        x.sum(2, 3);
    }

    // Пример перегрузки
    public int sum(int x, int y) {
        return x + y;
    }

    public int sum(int x, int y, int z) {
        return x + y + z;
    }

    public void blabla() {
        System.out.println("Blabla");
    }

}


class A extends Example {
    @Override // Переопределение
    public void blabla() {
        System.out.println("Hello");
    }

}

class B {

}
