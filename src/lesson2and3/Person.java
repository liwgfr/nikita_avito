package lesson2and3;

public class Person {
    // Инкапсуляция -> один из важнейших принципов ООП
    // private -> позволяет достичь инкапсуляцию
    // Зачем?
    // 1) Мы скрываем реализацию / делаем поля безопасными ...
    private String name; // Имя
    private int age; // возраст
    private String password;

    public Person(String name, int age, int qwe, int asd) {
        this.name = name;
        this.age = age;
        //  this.age = qwe;
        System.out.println(qwe);
        System.out.println(asd);
    }

    public Person(String name, int age, String password) {
        this.name = name; // this. -> ключевое слово, необходимое для того, чтобы различать поле и принимаемый параметр
        this.age = age;
        this.password = password;
    }

    // Конструкторов может быть сколь угодно (уникальных)
    public Person(String name) {
        this.name = name;
    }

    public Person(int age) {
        this.age = age;
    }

    // Как только мы создаем хотя бы один свой конструктор - пустой конструктор (который по умолчанию) исчезает
    public Person() {
    }

    public String getPassword() { // Getter -> просто возвращает поле
        if (this.name.equals("Sergey")) {
            return password;
        } else {
            return "Нет доступа";
        }
    }

    public void setPassword(String password) { // Setter -> позволяет менять поле
        // if (...)
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    // 1)
    // Наследование в Java -> принцип, который позволяет создавать связи Родитель-Дочерний класс.
    // Есть класс Animal. Есть классы Dog, Cat...
    // Dog extends (говорит о наследовании) Animal
    // Cat extends Animal
    // Это означает, что и Dog, и Cat перенимают методы (В том числе конструкторы) родительского класса Animal
    // Animal void voice() -> Dog & Cat -> voice()
    // В Java нет множественного наследования.
    // Dog extends Animal, Cat extends Dog, Animal extends Cat
    // *
    //* *
    // В Java все классы унаследованы от Object.
    // Переопределение - изменение поведения родительского метода
    // если метод static -> переопределять нельзя!!!
    @Override // Переопределяем
    public String toString() {
        return "Имя: " + name + " Возраст: " + age;
    }

    public String NormStroka() {
        return "Имя: " + name + " Возраст: " + age;
    }

    // При переопределении мы обязаны сохранить И сигнатуру метода, И его параметры (количество, тип данных, порядок принятия)
    @Override
    public boolean equals(Object o) {
        if (this == o) { // Если объект с которым мы сравниваем является тем же самым, тогда true
            return true; // p.equals(p)
        }
        if (o == null || getClass() != o.getClass()) {
            return false; // p.equals(null) -> false
            // getClass() -> возвращает класс (Person, Main)
            // p.equals(m) -> false
            // Если пытаемся сравнить дом с телефоном
        }

        Person person = (Person) o; // (int) double -> 25.3 -> 25
        // Пускай если выше все было ОК, тогда Person person = Приведенный к Person Object

        if (age != person.age) {
            return false;
        }
        // Примитивы сравниваются быстрее, чем ссылочные
        if (name != null) {
            if (!name.equals(person.name)) { // p1.equals(p2) if (p1 имя не равно p2
                return false;
            }
        } else {
            if (person.name != null) {
                return false;
            }
        }
        if (password != null) {
            return password.equals(person.password); // p1 пароль с p2
        }
        return person.password == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + age;
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }
}
