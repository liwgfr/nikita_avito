package lesson2and3;

import lesson2and3.Zoo.*;

public class Main {
    public static void main(String[] args) {
        Person p = new Person("Sergey", 19, "qwerty007");
        Person p1 = new Person("Sergey", 19, "qwerty007");
        System.out.println(p.getPassword());
        if (p.getAge() == 19) {
            System.out.println(p.toString()); // по умолчанию в выводе все приводится к toString()
        } else {
            System.out.println("blabla");
        }
        System.out.println(p.hashCode());
        System.out.println(p1.hashCode());
        System.out.println(p == p1); // false
        System.out.println(p.equals(p1)); // true

        Animal animal = new Animal("A", "B");
        Cat cat = new Cat("Joy", "Black");
        Cat cat1 = new Cat("Jack", "White", 10);
        DomesticCat domesticCat = new DomesticCat(cat1.getName(), cat1.getColor(), cat1.getAmountOfDots(), 3);
        Bird bird = new Bird("Michael", "Red", 12);
        System.out.println(cat);
        System.out.println(cat1);
        System.out.println(domesticCat);
        System.out.println(bird);

        Cat a = new DomesticCat("Sam", "Black and White", 10, 10);
        // DomesticCat s = (DomesticCat) new Cat("Sam", "Black and White", 10);
        // s.setAmountOfLegs(0);
        // Person p = (Person) obj
        // Специфицировать класс (Person) object (Object)
        // Cat > DomesticCat
        System.out.println(a.getAmountOfDots());
        Dog dog = new Dog("qwe", "asd");
        dog.hello(a);

        Animal[] animals = new Animal[7];
        animals[0] = a; // DomCat
        animals[1] = cat; // Cat
        animals[2] = cat1; // Cat
        animals[3] = domesticCat; // DomCat
        animals[4] = dog; // Dog
        animals[5] = bird; // Bird
        animals[6] = animal; // Animal
        for (Animal q : animals) {
            q.voice();
        }
        // Эквивалентно
        for (int i = 0; i < animals.length; i++) {
            animals[i].voice();
        }

        // Collection API
        // ArrayList (массив динамический), LinkedList, Stack, ArrayDeque
        // Различные реализованные структуры данных (Java 5+)

    }
}
