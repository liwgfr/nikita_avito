package lesson2and3.Zoo;

public class Dog extends Animal {
    private String favCommand;
    public Dog(String name, String color) {
        super(name, color);
    }

    @Override
    public String toString() {
        return "Dog{" +
                "favCommand='" + favCommand + '\'' +
                '}';
    }

    public String getFavCommand() {
        return favCommand;
    }
    public void hello(Cat c) {
        System.out.println("Hello " + c.getAmountOfDots());
    }

    @Override
    public void voice() {
        System.out.println("I am Dog");
    }
}
