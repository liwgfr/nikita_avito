package lesson2and3.Zoo;

public class Animal {
    // Полиморфизм - это означает, что у нашего родительского класса есть какой-то метод, а у дочерних классов поведение этого метода изменено
    private String name;
    private String color;

    public Animal(String name, String color) {
        this.name = name;
        this.color = color;
    }

    public void voice() {
        System.out.println("I am animal");
    }

    @Override
    public String toString() {
        return "Animal{" +
                "name='" + name + '\'' +
                ", color='" + color + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

}
