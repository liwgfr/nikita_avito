package lesson2and3.Zoo;

public class Cat extends Animal{
    private int amountOfDots;
    public Cat(String name, String color, int amountOfDots) {
        super(name, color);
        this.amountOfDots = amountOfDots;
    }
    public Cat(String name, String color) {
        super(name, color);
    }

    public int getAmountOfDots() {
        return amountOfDots;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "amountOfDots=" + amountOfDots +
                '}';
    }

    @Override
    public void voice() {
        System.out.println("I am Cat");
    }
}
