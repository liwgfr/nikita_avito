package lesson2and3.Zoo;

public class DomesticCat extends Cat { // DomCat -> Cat -> Animal
    private int amountOfLegs;
    public DomesticCat(String name, String color, int amountOfDots, int amountOfLegs) {
        super(name, color, amountOfDots);
        this.amountOfLegs = amountOfLegs;
    }

    @Override
    public String toString() {
        return "DomesticCat{" +
                "amountOfLegs=" + amountOfLegs +
                '}';
    }

    public int getAmountOfLegs() {
        return amountOfLegs;
    }

    public void setAmountOfLegs(int amountOfLegs) {
        this.amountOfLegs = amountOfLegs;
    }

    @Override
    public void voice() {
        System.out.println("I am Domestic Cat");
        //super.voice(); // super(param1, param2...) -> в конструктор родительского
        // super.вызов метода -> родительский метод
    }
}
