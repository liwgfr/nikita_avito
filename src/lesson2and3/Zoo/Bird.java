package lesson2and3.Zoo;

public class Bird extends Animal {
    private int age;

    public Bird(String name, String color, int age) {
        super(name, color); // ключевое слово super(param1, param2...) -> позволяет передать данные параметры в конструктор родительский
        this.age = age; // this -> с нашим, super -> с классом выше
        // this("qwe", "asd", 12); // аналогично super
    }

    @Override // Переопределяем поведение родительского (Animal) метода toString
    public String toString() {
        return "Цвет: " + getColor() + " Имя: " + getName() + " Возраст " + age;
    }


    public int getAge() {
        return age;
    }
    public int getAge(int x) {
        return age + x;
    }
//    public int getAge(String x) {
//        return ;
//    }

    @Override
    public void voice() {
        System.out.println("I am Bird");
    }
}
