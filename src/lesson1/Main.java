package lesson1;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // Примитивные типы данных
        int a; // 5 байт (-2^31...2^31-1) -1? -> 0 +
        short b; // -2^15...2^15-1
        byte c; // -2^7...2^7-1
        long d; // -2^63...2^63
        // Выше - все целые числа
        char e; // 0...65535 (2^15-1). char == целое число. Unicode.
        char l = 97; // берется символ из этого числа из таблицы Unicode ('a')
        // 'a' (char), "w" (String)
        char x = 'f';
        System.out.println((int) x); // cast преобразование к типу данных

        double q; // -2^63...2^63-1 (12-14 знаков после запятой)
        float bqw; // память как у int (6-7 знаков после запятой)

        boolean asdsd; // 1 byte true/false

        // Ссылочные типы данных
        // Не относятся к 8 примитивам
        // Пишутся с большой буквы
        // или присутствует ключевое слово new
        // new говорит, что выделяем участок в памяти

        int[] ar = new int[2];
        String s;
        Scanner p; // для считывания данных либо из клавиатуры, либо из файла
        Main m;
        // Примитивы хранятся в стэке (Stack)
        // Ссылочные хранятся в куче (Heap)
        // Stack -> RAM (доступ быстрый, но места мало) 4gb -> Primitive
        // Heap -> HDD (доступ медленнее, но места больше) 2tb -> Все остальное
        // В джаве наиболее часто используется примитивы -> чтобы джава работала быстрее и запускалась не за 100с, а за 2, было решено вынести 8 типов в Stack

        // Массив - набор однотипных данных
        // 1
        int[] j; // заведи переменную j и скажи, что она массив целых чисел
        // ...
        // ...
        j = new int[10]; // new int[const] -> это размер массива
        // У всех ссылочных по умолчанию значение null
        // по умолчанию массив заполнен 0 (int, short...), 0.0 (double/float), String[] (null), boolean[] (false) ...
        // 2
        int[] qwe = new int[10];
        // 3
        int[] po = new int[]{1, 2, 3, 5, 100}; // size 5
        int[] v = {1, 2, 3, 5, 100}; // size 5
        // Массивы нумеруются с 0
        qwe[1] = 5;
        // если мы поставим размер массива отрицательный, код скомпилируется, но будет NegativeArraySizeException
        // мы можем также поставить размер массива 0, но он пустой, положить ничего не сможем
        // если мы обращаемся за границы массива (ar[10] -> IndexOutOfBoundsException)

        Scanner in = new Scanner(System.in); // System.in говорит, что считываем с клавиатуры. new Scanner() -> конструктор, которые принимает другой класс System.in

//        int num1 = in.nextInt(); // считывает число
//        int num2 = in.nextInt(); // считывает число
//        System.out.println(num1 + num2); // println выводит на следующую строку

        // 1
        // String pool
        // Строка заносится в pool (образно говоря, это Stack внутри Heap; cache)
        String str1 = "Hello World"; // Hello World -> pool
        String str2 = "Hello World"; //
        // 2
        // new -> явно говорит джаве выделить новую память
        String str3 = new String("Hello World");
        String str4 = new String("Hello World");
        System.out.println(str1 == str2); // true
        System.out.println(str3.equals(str4)); // false
        // Методы у строк
        // Строка по сути это массив из символов
        // str1.equals(str2) -> сравнивает строки по значению
        // str1.trim() -> обрезает строку (то есть убирает лишние пробелы после строки) "Hello   ";
        // toUpperCase()
        // toLowerCase()
        // str.replace('l', 'w'); -> заменяет в строке все символы 'l' -> 'w'
        // str.replaceAll("[a-zA-Z]+|[0-9]+", ""); -> принимает RegEx
        System.out.println("Hello     Wo rl     d    ".replaceAll("\\s+", ""));
        // split("regex") -> возвращает массив из String[]
        System.out.println(Arrays.toString("Hello     Wo rl     d    ".split("\\s+")));
        // Arrays.toString(массив) позволяет вывести массив в виде строки (иначе получили бы ссылку на него)
        // аналог - вывод в цикле
        // s.charAt(index) -> возвращает символ по позиции (ar[i])
        // substring(startPos, endPos) -> подстрока (String)
        // length() -> длина строки
        // "Hello World".contains("Hello") -> true/false

////        int value = in.nextInt();
//        // +, -, *, / -> div, % -> mod
//        System.out.println(5 / 2); // -> 2
//        System.out.println(5 % 2); // -> 1
//        if (value % 2 == 0) {
//            System.out.println("Qwe" + " " + "Done");
//            System.out.println("zxczxc");
//        } else {
//            System.out.println("zxczxc");
//        }
//
//        switch (value) {
//            case 1:
//                System.out.println("Hello");
//            case 2:
//                System.out.println("qweqwe");
//                // ...
//        }
//        switch (value) {
//            case 1 -> System.out.println("Hello"); // Lambda
//            case 2 -> System.out.println("Hello");
//        }
//
//        // if (*выражение1* && *выражение2*) && -> Логическое И Lazy операнд
//        // if (*выражение1* & *выражение2*) & -> Логическое И
//
//        // if (*выражение1* || *выражение2*) || -> Логическое ИЛИ Lazy
//        // if (!(*выражение1*) | *выражение2*) | -> Логическое ИЛИ
//        // boolean flag = false;
//        // if (!flag)  то же самое что и if (flag == false) -> ! отрицание
//
//        // 7 & 2 -> побитовый И (111, 010 -> 010 -> 2)
//        // 7 | 2 -> побитовый ИЛИ (111, 010 -> 111 -> 7)
//        // 7 ^ 2 -> побитовый исключающее ИЛИ (xor) (111, 010 -> 101 -> 5)
//
//        // Циклы:
//        int idx = 0;
//        while (value < 100) {
//            // if (..)
//            idx++; // постфиксный инкремент, ar[++idx] -> ar[1], ar[idx++] -> ar[0] -> 1
//            System.out.println(idx);
//            value++;
//        }
//        for (int i = 0; i <= value * 2; i++) {
//
//        }
//        for (int i = 0; ; i++) {
//
//        }

//        int num1 = in.nextInt();
//        int[] arr = new int[num1];
//        for (int i = 0; i < arr.length; i++) {
//            arr[i] = in.nextInt();
//            System.out.print(arr[i] + " ");
//        }

        // System.out.println(Integer.MAX_VALUE); // 2^31-1
        int asd = 2147483647;
        int asd2 = -2147483648;
        // Когда происходит переполнение типа данных, значение данного типа становится max/min +1 -> Integer.MIN_VALUE (-2^31)
        System.out.println(asd2 - 1);
        long vbn = asd + 2147483647;
        int dsadsa = (int) vbn;
        System.out.println(dsadsa);
        int dig = 10; // 10.0
        double dob = dig;
        System.out.println(dob);
        int h = (int) 14.999;
        System.out.println(h);
        System.out.println();

    }
}
