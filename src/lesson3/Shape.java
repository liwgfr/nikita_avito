package lesson3;

public abstract class Shape {
    protected int value;
    // abstract -> говорит, что данный класс является абстрактным
    // Класс является абстрактным (abstract) только тогда, когда в нем присутствует хотя бы один abstract метод
    // Здесь возникает необходимость абстракции
    public abstract double square(int x); // абстрактный => реализовываться здесь он НЕ будет! Дочерние классы сами его сделают

    public void hello() {
        System.out.println("I am Shape");
    }

    public Shape(int value) {
        this.value = value;
    }
}
