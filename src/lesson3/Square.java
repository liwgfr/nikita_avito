package lesson3;

public class Square implements SquareInterface {
    @Override // Определяем поведение родительского абстрактного метода
    public double square(int x) {
        return x * x;
    }

    @Override
    public void SendMessage(String text) {
        // code
    }

    @Override
    public void SendVideo() {
        // code
    }

//    @Override
//    public String hello(String x) {
//        return x;
//    }
}
