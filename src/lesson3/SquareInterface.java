package lesson3;


// Интерфейс это по сути тот же самый абстрактный класс
// Отличия от класса:
// В контексте Java 8+
// 1) В интерфейсах НЕЛЬЗЯ создавать конструкторы
// 2) от классов можно наследоваться единожды (extends Shape, Model, ...), в то время как имплементировать интерфейсы можно множественно (implements AInterface, BInterface, CIntreface...)
// 3) В интерфейсах все поля public static final и изменить это нельзя
// До Java 8
// В интерфейсах не могло быть реализации методов (в отличие от абстрактных классов)
public interface SquareInterface {
    // В интерфейсах все методы по умолчанию и единственный это public
    double square(int x);
    default String hello(String x) {
        return "No Input";
    }
    void SendMessage(String text); // TelegramBot, VkBot, DiscordBot...
    void SendVideo();
    // default в интерфейсах говорит, что данный метод (если его НЕ реализовали) будет выполнять описанную логику
}
