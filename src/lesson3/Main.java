package lesson3;

public class Main {
    public static void main(String[] args) {
        Circle circle = new Circle(32);
        Square square = new Square();
        System.out.println(circle.square(10));
        System.out.println(square.square(5));
        System.out.println(square.hello("q"));

    }
    // Зачем нужны абстрактные классы, если есть интерфейсы??
    // Проблема обратной совместимости: Код, написанный на Java 17 должен иметь возможность быть скомпилированным на Java 8
    
}
