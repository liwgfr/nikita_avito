package lesson3;

public class Circle extends Shape {


    public Circle(int value) {
        super(value);
    }

    @Override // Определяем поведение родительского абстрактного метода
    public double square(int r) {
        value = 1;
        return Math.PI * r * r;
    }
}
