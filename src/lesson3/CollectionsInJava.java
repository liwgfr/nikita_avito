package lesson3;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class CollectionsInJava {
    // Collection API -> были введены начиная с Java 5+
    // API -> Application Programming Interface
    // Набор каких-то методов, которые упрощают взаимодействие с библиотекой
    // Framework и Библиотека -> фреймворк это сборник библиотек и/или других фреймворков, библиотека это сборник классов
    // Библиотека написана на одном языке (например, если библиотека для джавы, то разумеется, что endpoint'ы (точки взаимодействия) написана на Java)
    // Фреймворк как правило состоит из нескольких языков
    // Collection -> структуры данных (List, Queue, Stack, Map...)
    // List, Queue, Set... (кроме Map) -> интерфейсе
    // Во главе этих интерфейсов стоит Collection (I)
    // ArrayList (C) -> реализует интерфейс List. В контексте Java это динамический массив (массив без строгого размера -> увеличивается по мере заполнения)
    // У ArrayList есть различные полезные методы
    // add(...) -> добавляет в ArrayList значение
    // .get(int idx) -> возвращает значение по индексу
    // toString() -> ArrayList в строку
    // list.contains(5) -> возвращает true, если в ArrayList есть 5
    // list.remove(int idx) -> удаляет по индексу элемент
    // list.size() -> возвращает размер коллекции (аналог array.length)

    public static void main(String[] args) {
        List<String> l = new CopyOnWriteArrayList<>();
        // Для коллекций можно (и даже желательно) указывать тип данных с которыми работаем <String> или <Integer> или <Double> или <Character> (char)
        ArrayList<String> list = new ArrayList<>(); // по умолчанию Object не int, а Integer
        // <>
        list.add("3");
        list.add("10");
        list.add("55");
        System.out.println(list.get(1));
        System.out.println(list.contains("55"));
        System.out.println(list.contains("54"));
        list.remove("3"); // int НЕ extends Object
        System.out.println(list);

    }
}
